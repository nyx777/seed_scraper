import json
import csv
import re
import logging

logger = logging.getLogger("data_loader")

def load_company_to_sese_sku_data(matches_file):
    """Loads company-to-sese sku mappings from '<data_dir>/Matched varieties from `csvlink` - *.csv'"""
    #
    # this csv file is a compilation of company-to-sese-sku matches:
    # "Company Prefix","Company SKU","SESE SKU"
    # "<company_prefix_1>","<company_sku_1","<sese_sku_1>"
    # "<company_prefix_1>","<company_sku_2","<sese_sku_2>"
    # "<company_prefix_2>","<company_sku_3","<sese_sku_3>"
    # "<company_prefix_2>","<company_sku_4","<sese_sku_4>"
    #
    # returns:
    # (
    #     ("<company_prefix_1>","<company_sku_1","<sese_sku_1>"),
    #     ("<company_prefix_1>","<company_sku_2","<sese_sku_2>"),
    #     ("<company_prefix_2>","<company_sku_3","<sese_sku_3>"),
    #     ("<company_prefix_2>","<company_sku_4","<sese_sku_4>"),
    #     ...
    # )

    #data_files = tuple(p for p in os.listdir(data_dir)
    #                   if "Matched varieties from `csvlink` - " in p
    #                   and p.endswith(".csv")
    #                   and not p.startswith("."))
    #if data_files != None and len(data_files) > 0:
    #    data_file_newest = natsorted(data_files)[-1]
    #    return tuple(tuple(row) for row in csv.reader(open(os.path.join(data_dir, data_file_newest)).readlines()))[1:]
    #else:
    #    return None
    logger.info("Reading CSV data from: " + matches_file)
    return tuple(tuple(row) for row in csv.reader(open(matches_file).readlines()))[1:]

def load_company_listing_data(listings_file):
    """Loads data from the newest listings.<date>.jl file."""
    #data_files = [jl_path for jl_path in os.listdir(data_dir)
    #              if "sese_listings" not in jl_path
    #              and "listings." in jl_path
    #              and ".jl" in jl_path
    #              and jl_path.startswith(".") == False]
    #data_file_newest = natsorted(data_files)[-1]
    logger.info("Reading JSON-Lines data from: " + listings_file)
    return tuple( json.loads(row) for row in open(listings_file).readlines() )

def load_weight_to_seeds_data(file_path):
    logger.info("Reading CSV data from: " + file_path)
    data = tuple(csv.reader(open(file_path).readlines()))
    dict_data = {row[0]:{"type": row[1], "weight": row[2], "seeds": row[3]} for row in data[1:] if re.search("[a-z]", row[2].lower()) == None}

    for sku, data in dict_data.items():
        dict_data[sku]["weight"] = float(data["weight"])
        if "-" in data["seeds"]:
            nums = [float(x) for x in data["seeds"].split("-")]
            largest = sorted(nums)[-1]
            dict_data[sku]["seeds"] = float(largest)
        else:
            dict_data[sku]["seeds"] = float(data["seeds"])

# averages seed range
##    for sku, data in dict_data.items():
##        dict_data[sku]["weight"] = float(data["weight"])
##        if "-" in data["seeds"]:
##            nums = [float(x) for x in data["seeds"].split("-")]
##            avg = sum(nums)/len(nums)
##            dict_data[sku]["seeds"] = float(avg)
##        else:
##            dict_data[sku]["seeds"] = float(data["seeds"])

    return dict_data
