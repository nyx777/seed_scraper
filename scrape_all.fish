#!/usr/bin/env fish
rm -f listings.jl && rm -f log.txt &&\
    scrapy crawl bc_products -o listings.jl &| tee -a log.txt &&\
    scrapy crawl bi_products -o listings.jl &| tee -a log.txt &&\
    scrapy crawl fs_products -o listings.jl &| tee -a log.txt &&\
    scrapy crawl hm_products -o listings.jl &| tee -a log.txt &&\
    scrapy crawl hv_products -o listings.jl &| tee -a log.txt &&\
    scrapy crawl js_products -o listings.jl &| tee -a log.txt &&\
    scrapy crawl rg_products -o listings.jl &| tee -a log.txt &&\
    scrapy crawl se_products -o listings.jl &| tee -a log.txt &&\
    scrapy crawl ss_products -o listings.jl &| tee -a log.txt &&\
    scrapy crawl st_products -o listings.jl &| tee -a log.txt &&\
    scrapy crawl ts_products -o listings.jl &| tee -a log.txt
