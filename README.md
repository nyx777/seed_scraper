# seed_scraper
This repository contains code for scraping seed price data. It utilizes `python3` and the web scraping library [`scrapy`](https://scrapy.org/). Note that it is assumed a Unix-like OS (Linux or Mac OSX) will run the code.

The following sites can be scraped...

|Company|Crawler Name|
|-|-|
|[Botanical Interests](https://www.botanicalinterests.com/)|bi_products|
|[Fedco Seeds](https://www.fedcoseeds.com/)|fs_products|
|[High Mowing Seeds](https://www.highmowingseeds.com/)|hm_products|
|[Johnny's Seeds](https://www.johnnyseeds.com/)|js_products|
|[Renee's Garden Seeds](https://www.reneesgarden.com/)|rg_products|
|[Sow True Seeds](https://sowtrueseed.com/)|st_products|
|[Territorial Seeds](https://territorialseed.com/)|ts_products|
|[Hudson Valley Seeds](https://hudsonvalleyseed.com/)|hv_products|
|[Seed Savers Exchange](https://www.seedsavers.org/)|ss_products|


Sample data can be found in the [`data`](https://gitlab.com/nyx777/seed_scraper/-/tree/main/data) directory. Crawler code can be found in the [`spiders`](https://gitlab.com/nyx777/seed_scraper/-/tree/main/seed_scraper/spiders) directory.

## Installation

1. Ensure `python3` is installed.
2. Clone this repository and enter its directory.
3. Create and activate a virtual environment.
4. Install dependencies.
	```
	git clone git@gitlab.com:nyx777/seed_scraper.git
	cd seed_scraper
	python3 -m venv ./
	source bin/activate
	python3 -m pip install -r requirements.txt 
	```

## Using the Scraper
From within the `seed_scraper` directory, activate the virtual environment and run the desired crawler.
```
cd seed_scraper
source bin/activate
scrapy crawl <crawler_name> -O <crawler_name>.jl
```
This will run `<crawler_name>` and write the scraped data to a `JSON Lines` file called `<crawler_name>.jl`.

### Scraping Territorial Seeds
The crawler which scrapes Territorial Seeds collects product links from a PDF catalog converted to an HTML document. The conversion can be performed with the command `pdftohtml`. An example of command which performs the conversion is as follows...

```
pdftohtml <input-file>.pdf <output-file>.html -s -i
```

Note that `<output-file>.html` is encoded in `ts_spider.py` as the first URL to parse.

## Uploading to Google Sheets
The scraped data can be converted from the `JSON Lines` format to a `CSV` file, which can then be imported into Google Sheets. The `shell script` named `<prefix>-jq-command.sh` within this repository utilizes the open source tool [`jq`](https://stedolan.github.io/jq/) to perform the conversion.

1. Install [`jq`](https://stedolan.github.io/jq/)
2. Run the following `shell script`
	```
	cat <crawler_name>.jl | ./scripts/<crawler_prefix>-jq-command.sh > <crawler_name>.csv
	```
3. Open a Google Sheets document
4. Click `File -> Import -> Upload`
5. Upload `<crawler_name>.csv`
6. Deselect `Convert text to numbers, dates, and formulas`
7. Click `Import data`
