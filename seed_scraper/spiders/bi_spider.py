import scrapy
import logging
import re

from seed_scraper.items import SeedListing

logger = logging.getLogger(__name__)

class BISpider(scrapy.Spider):
    '''Scrapes seed product listings from Botanical Interests.'''
    company_prefix = "bi"
    company_name = "Botanical Interests"
    name = company_prefix + "_products"
    list_base_url = "https://www.botanicalinterests.com/category/"
    product_base_url = "https://www.botanicalinterests.com/product/"
    start_urls = [
        list_base_url + "view-all-vegetables/1",
        list_base_url + "view-all-flowers/1",
        list_base_url + "View-All-Herbs/1",
    ]

    def parse(self, response):
        def is_product_list_page(response):
            return response.url.startswith(self.list_base_url)
        def is_product_info_page(response):
            return response.url.startswith(self.product_base_url)

        if is_product_list_page(response):
            for i in self.crawl_next_list_page(response): yield i
            for i in self.crawl_product_info(response): yield i
        elif is_product_info_page(response):
            for i in self.parse_product_info(response): yield i

    def crawl_next_list_page(self, response):
        '''Finds and crawls link to next page on product lists.'''
        page_nums = response.css(".curPage + .pagenumber")
        if len(page_nums) > 0:
            next_page = page_nums[0].css("a").attrib["href"]
            if next_page is not None:
                next_page = response.urljoin(next_page)
                yield scrapy.Request(next_page, callback=self.parse)

    def crawl_product_info(self, response):
        '''Finds and crawls links to product info pages.'''
        def has_price(el):
            return len(el.css("div.order div.price")) > 0

        for product in filter(has_price, response.css("section.product")):
            product_page = response.urljoin(
                product.css("div.content a").attrib["href"])
            yield scrapy.Request(product_page, callback=self.parse)

    def get_variety(self, response):
        content = response.css(".product .info .content")
        return content.xpath("h1/text()").get().strip()

    def get_categories(self, response):
        content = response.css(".product .info .content")
        categories = content.xpath("h2/text()").get()
        if categories != None:
            return categories.strip().split(", ")
        else:
            return []

    def is_single_product_page(self, response):
        content = response.css(".product .info .content")
        skus = content.xpath("div[@class=\"sku\"]/text()")
        return len(skus) == 1

    def parse_product_info(self, response):
        if self.is_single_product_page(response):
            for i in self.parse_single_product(response): yield i
        else:
            for i in self.parse_multiple_products(response): yield i

    def parse_single_product(self, response):

        def extract_quantity(text):
            quantity_num, quantity_unit = text.split(" ")[:2]
            if len(quantity_num) != 0:
                quantity_num = float(quantity_num)
                if quantity_unit == "mg":
                    quantity_num = quantity_num / 1000
                    quantity_unit = "grams"
                elif quantity_unit == "gram":
                    quantity_unit = "grams"

            return (quantity_num, quantity_unit.lower())

        content = response.css(".product .info .content")
        skus = content.xpath("div[@class=\"sku\"]/text()")
        data = content.xpath("div[@class=\"price\"]/span/text()")

        sku = skus[0].get().replace("#", "")
        variety = self.get_variety(response)
        price = float(data[0].get().replace("$", ""))
        quantity_num, quantity_unit = extract_quantity(data[1].get())
        in_stock = re.search("In Stock", content.get()) != None

        categories = self.get_categories(response)
        organic = None
        heirloom = None
        native = None
        packet_type = ""

        if categories != None:
            try:
                categories.index("Organic")
                organic = True
            except ValueError:
                organic = False

            try:
                categories.index("Heirloom")
                heirloom = True
            except ValueError:
                heirloom = False

            try:
                categories.index("Native")
                native = True
            except ValueError:
                native = False
        else:
            categories = []

        if variety.find("Collection") != -1:
            categories.append("Collection")

        yield SeedListing(
            company_prefix = self.company_prefix,
            company_name = self.company_name,
            sku = sku,
            variety = variety,
            price = price,
            quantity = quantity_num,
            unit = quantity_unit,
            in_stock = in_stock,
            attributes = {
                "categories": categories,
                "organic":  organic,
                "heirloom":  heirloom,
                "native":  native,
                "packet_type": packet_type,
            },
            url = response.url,
        )

    def parse_multiple_products(self, response):

        def extract_sku(text):
            match = re.search("#\d+", text)
            if match != None:
                return match.group().replace("#", "")

        def extract_price(text):
            match = re.search("\$\d+\.\d+", text)
            if match != None:
                return float(match.group().replace("$", ""))

        def extract_quantity(text):
            match = re.search("\d+ \w+", text)
            if match != None:
                data = match.group().split(" ")
                quantity_num = float(data[0])
                quantity_unit = data[1]
                if quantity_unit == "mg":
                    quantity_num = quantity_num / 1000
                    quantity_unit = "grams"
                elif quantity_unit == "gram":
                    quantity_unit = "grams"
                return (quantity_num, quantity_unit.lower())
            else:
                return (None, None)

        for row in response.css(".product .info .order .row"):

            text = row.xpath("li/text()")[1].get().strip()
            sku = extract_sku(text)
            variety = self.get_variety(response)
            price = extract_price(row.css("li.child-price").get())
            quantity_num, quantity_unit = extract_quantity(text)
            packet_type = row.xpath("li/strong/text()").get()
            categories = self.get_categories(response)
            organic = None
            heirloom = None
            native = None

            in_stock = re.search("Out of Stock", row.get()) == None
            if categories != None:
                if packet_type.find("Conventional") != -1:
                    categories = list(filter(lambda z: z.find("Organic") == -1, categories))
                    categories.append("Conventional")
                    organic = False
                elif packet_type.find("Organic") != -1:
                    try:
                        categories.index("Organic")
                    except ValueError:
                        categories.append("Organic")
                    organic = True
                else:
                    try:
                        categories.index("Organic")
                        organic = True
                    except ValueError:
                        organic = False

                try:
                    categories.index("Heirloom")
                    heirloom = True
                except ValueError:
                    heirloom = False

                try:
                    categories.index("Native")
                    native = True
                except ValueError:
                    native = False
            else:
                categories = []

            if variety.find("Collection") != -1:
                categories.append("Collection")

            yield SeedListing(
                company_prefix = self.company_prefix,
                company_name = self.company_name,
                sku = sku,
                variety = variety,
                price = price,
                quantity = quantity_num,
                unit = quantity_unit,
                in_stock = in_stock,
                attributes = {
                    "categories": categories,
                    "organic":  organic,
                    "heirloom":  heirloom,
                    "native":  native,
                    "packet_type": packet_type,
                },
                url = response.url,
            )
