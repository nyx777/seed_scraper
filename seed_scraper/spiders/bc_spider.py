import scrapy
import logging
import re
import json

from seed_scraper.items import SeedListing

logger = logging.getLogger(__name__)

class BCSpider(scrapy.Spider):
    company_prefix = "bc"
    company_name = "Baker Creek"
    name = company_prefix + "_products"
    base_url = "https://www.rareseeds.com/"
    vegetables_url = base_url + "store/vegetables"
    category_links = []
    product_links = []

    def start_requests(self):
        yield scrapy.Request(url=self.vegetables_url, callback=self.parse)

    def parse(self, response):
        logger.debug("Response url is: " + response.url)
        if response.url == self.vegetables_url:
            for i in self.crawl_categories(response): yield i
        elif response.url in self.category_links:
            for i in self.crawl_products(response): yield i
        else:
            for i in self.parse_product(response): yield i

    def crawl_categories(self, response):
        self.category_links = [x.xpath("@href").get() for x in response.css("a.cat--name")]
        logger.debug("Crawling category links:\n" + "\n".join(self.category_links))
        for link in self.category_links:
            yield scrapy.Request(url=link, callback=self.parse)

    def crawl_products(self, response):
        logger.debug("Crawling products from: " + response.url)
        product_links = [x.xpath("@href").get() for x in response.css("div.product--item_image a")]
        for link in product_links:
            if link not in self.product_links:
                self.product_links.append(link)
                yield scrapy.Request(url=link, callback=self.parse)
        yield None

    def parse_product(self, response):
        logger.debug("Parsing product from: " + response.url)

        sku = response.css("div.product.attribute.sku div.value").xpath("text()").get()
        variety = response.css("h1.page-title.current-title span").xpath("text()").get()
        variety = variety.strip()
        price = response.css("span.price-container span span").xpath("text()").get().strip("$")
        quantity = response.css("span.minimum-seed-count").xpath("text()").get()
        if quantity != None:
            quantity = quantity.split(": ")[-1]
        else:
            quantity = ""
        unit = "seeds"
        in_stock = len(response.css("div.stock.available")) > 0
        url = response.url
        attributes = None

        yield SeedListing(
            company_prefix = self.company_prefix,
            company_name = self.company_name,
            sku = sku,
            variety = variety,
            price = price,
            quantity = quantity,
            unit = unit,
            in_stock = in_stock,
            attributes = attributes,
            url = url,
        )
