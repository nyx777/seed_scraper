import scrapy
import logging
import re
import json
from functools import reduce
from pprint import pformat

from seed_scraper.items import SeedListing

logger = logging.getLogger(__name__)

class HVSpider(scrapy.Spider):
    company_prefix = "hv"
    company_name = "Hudson Valley Seeds"
    name = "hv_products"
    base_url = "https://hudsonvalleyseed.com"
    collections = [
        "vegetables",
        "herbs",
        "flowers",
    ]

    def start_requests(self):
        for c in self.collections:
            yield scrapy.Request(url=self.base_url + "/collections/" + c + "?view=all&sort_by=title-ascending", callback=self.parse)

    def parse(self, response):
        logger.debug("Response url is: " + response.url)
        if "products" in response.url:
            for i in self.parse_product(response): yield i
        elif "collections" in response.url:
            for i in self.crawl_products(response): yield i

    def crawl_products(self, response):
        logger.debug("Crawling products from: " + response.url)
        product_els = response.css("div.product-layout.product-list")
        logger.debug("Products found: " + str(len(product_els)))
        for el in product_els:
            product_link = self.base_url.rstrip("/") + el.css("h4.product-name a").xpath("@href").get()
            yield scrapy.Request(url=product_link, callback=self.parse)

    def parse_product(self, response):
        logger.debug("Parsing product from: " + response.url)
        variety = response.css("h1.product-name").xpath("text()").get()
        category = ""
        if len(response.css("ul.breadcrumb a")) >= 2:
            category = response.css("ul.breadcrumb a")[1].xpath("text()").get()
        url = response.url

        listings_table = response.css("table#form_buy tbody tr")
        logger.debug("Number of offerings: " + str(len(listings_table)))
        for row in listings_table:
            data = row.css("td")
            logger.debug("\n" + pformat(data))
            quantity = data[0].xpath("text()").get()
            price = data[1].xpath("text()").get().lstrip("$")
            in_stock = len(data[3].css("span.out-of-stock")) == 0
            organic = ""

            unit = ""
            if "Seeds" in quantity:
                unit = "seeds"
                if "ORG" in quantity:
                    organic = True
                quantity = quantity.split(" ")[0]
            elif "seeds" in quantity:
                unit = "seeds"
                quantity = quantity.rstrip(" seeds")
            elif "Ounce" in quantity:
                unit = "oz"
                quantity = quantity.rstrip(" Ounce")
            elif "Ounces" in quantity:
                unit = "oz"
                quantity = quantity.rstrip(" Ounces")
            elif "oz" in quantity:
                unit = "oz"
                quantity = quantity.rstrip(" oz")
            elif "Pound" in quantity:
                unit = "lb"
                quantity = quantity.rstrip(" Pound")
            elif "Pounds" in quantity:
                unit = "lb"
                quantity = quantity.replace(" Pounds", "")
            elif "tuber" in quantity:
                unit = "tuber"
                quantity = quantity.rstrip(" tuber")
            elif "tubers" in quantity:
                unit = "tubers"
                quantity = quantity.rstrip(" tubers")
            elif "bulbs" in quantity:
                unit = "bulbs"
                quantity = quantity.rstrip(" bulbs")
            elif "Unit" in quantity:
                unit = "unit"
                quantity = quantity.rstrip(" Unit")

            quantity = quantity.replace(",", "")

            yield SeedListing(
                company_prefix = self.company_prefix,
                company_name = self.company_name,
                sku = variety,
                variety = variety,
                price = price,
                quantity = quantity,
                unit = unit,
                in_stock = in_stock,
                attributes = {
                    "category": category,
                    "organic": organic,
                },
                url = url,
            )
        yield None
