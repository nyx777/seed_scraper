import scrapy
import logging
import re
import json
from functools import reduce

from seed_scraper.items import SeedListing

logger = logging.getLogger(__name__)

class RGSpider(scrapy.Spider):
    '''Scrapes seed product listings from Johnny's Seeds.'''
    company_prefix = "rg"
    company_name = "Renee's Garden"
    name = company_prefix + "_products"
    base_url = "https://www.reneesgarden.com/"
    home_url = base_url + "collections/vegetables"

    def start_requests(self):
        yield scrapy.Request(url=self.home_url, callback=self.parse)

    def parse(self, response):
        def is_home(response):
            return response.url == self.home_url
        def is_collection(response):
            return not is_home(response) and response.url.startswith(self.base_url+"/collections")
        def is_product(response):
            return not is_home(response) and response.url.startswith(self.base_url+"/products")
        if is_home(response):
            for i in self.crawl_collections(response): yield i
        if is_collection(response):
            for i in self.crawl_product(response): yield i
        if is_product(response):
            for i in self.parse_product(response): yield i

    def crawl_collections(self, response):
        logger.debug("Crawling collections from: " + response.url)
        for x in [self.base_url + x.get() for x in response.css("a").xpath("@href")
                  if x.get().startswith("/collections/")]:
            yield scrapy.Request(url=x, callback=self.parse)

    def crawl_product(self, response):
        logger.debug("Crawling products from: " + response.url)
        for x in [self.base_url + x.get() for x in response.css("a.collection-item-text").xpath("@href")
                  if x.get().startswith("/products/")]:
            yield scrapy.Request(url=x, callback=self.parse)

    def parse_product(self, response):
        logger.debug("Parsing product from: " + response.url)

        sku = response.xpath("//span[@class='sku']/text()").get().lstrip("#")
        variety = response.css(".product-text-section h1").xpath("text()").get()
        category = response.css(".product-text-section h2 i").xpath("text()").get()
        if category is None: category = ""
        in_stock = len(response.css("div.sold-out-season-product")) == 0 and len(response.css("div.out-of-stock-product")) == 0
        price = ""
        unit = ""
        quantity = ""
        seeds = ""
        url = response.url

        if not in_stock: logger.debug("Product not in stock: " + response.url)

        p = None
        p = response.css("div.purchase span.r-price_sale").xpath("text()").get()
        u = response.css("div.seed-count").xpath("text()").get()
        if u != None: u = re.search("[A-Z].*", u)
        if u != None: quantity = u.group()

        for x in quantity.split(" / "):
               logger.debug("Quantity '/' split: " + x)
               if x.startswith("Seed Count: "):
                   x = x.lstrip("Seed Count: ")
                   seeds = x
               if x.startswith("Weight: "):
                   x = x.lstrip("Weight: ")
                   y = x.split(" ")
                   quantity = y[0]
                   if y[1] == "gm" or y[1] == "gms":
                       unit = "g"
        if p != None: p = re.search("[0-9]+\.[0-9]+", p)
        if p != None: price = p.group()

        if price == "":
            purchase_spans = response.css("div.purchase span")
            if len(purchase_spans) > 1:
                price_search = re.search("[0-9]+\.[0-9]+", purchase_spans[0].xpath("text()").get())
                if price_search != None:
                    price = price_search.group()

        yield SeedListing(
            company_prefix = self.company_prefix,
            company_name = self.company_name,
            sku = sku,
            variety = variety,
            price = price,
            quantity = quantity,
            unit = unit,
            in_stock = in_stock,
            attributes = {
                "category": category,
                "seeds": seeds,
            },
            url = url,
        )
