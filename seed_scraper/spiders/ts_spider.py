import scrapy
import logging
import re
import json
import os
import pprint
from functools import reduce

from seed_scraper.items import SeedListing

logger = logging.getLogger(__name__)

class TSSpider(scrapy.Spider):
    '''Scrapes seed product listings from Johnny's Seeds.'''
    company_prefix = "ts"
    company_name = "Territorial Seeds"
    name = company_prefix + "_products"
    catalog_url = "file://"+ os.path.realpath("data/Territorial Seeds - 2022 Spring Catalog with links v2.html")
    website_url = "https://territorialseed.com/"

    def start_requests(self):
        yield scrapy.Request(url=self.catalog_url, callback=self.parse)

    def parse(self, response):
        logger.debug("Response url is: " + response.url)
        if response.url.startswith("file:///"):
            for i in self.crawl_products(response): yield i
        elif response.url.startswith(self.website_url + "products/"):
            for i in self.parse_product(response): yield i

    def crawl_products(self, response):
        logger.debug("Crawling products from: " + response.url)
        product_links = []
        for x in [
                x.get()
                for x in response.xpath("//a/@href")
                if x.get().startswith("https://territorialseed.com/products/")
        ]:
            if x not in product_links:
                product_links.append(x)
        for p in product_links:
            yield scrapy.Request(url=p, callback=self.parse)

    def parse_product(self, response):
        logger.debug("Parsing product from: " + response.url)
        UNWANTED_CATEGORIES = [
            "HEAT CABLE",
            "DRY",
            "TOOL",
            "LIQUID",
            "PEST",
            "CLOCHE",
            "FUNGICIDE",
            "ROW COVER",
            "GREENHOUSE",
            "PEST CONTROL",
            "COLDFRAME",
            "SOIL",
            "WEEDING/CULTIVATING",
            "HARVESTING",
            "POLLINATION",
            "WATERING",
            "INSECT",
            "TAG",
            "POT/PLANTER",
            "SEEDER",
            "COMPOSTER",
            "LIGHTING",
            "KIT",
            "FLAT/INSERT",
            "WEED BARRIER",
            "VEGETABLE STORAGE",
            "COLLECTION",
            "SUPPORT",
            "PLASTIC",
            "HEAT MAT",
        ]
        variety = response.css("h1.product__title").xpath("text()").get()
        meta_script_els = [x.get() for x in response.css("script") if "var meta" in x.get()]
        if len(meta_script_els) == 1:
            logger.debug("Found script tag with metadata")
            meta_var_lines = [x for x in meta_script_els[0].split("\n") if "var meta" in x]
            if len(meta_var_lines) == 1:
                logger.debug("Found meta var line")
                meta_json = json.loads(meta_var_lines[0].lstrip("var meta = ").rstrip(";"))
                if meta_json is not None:
                    logger.debug("Meta json:\n" + pprint.pformat(meta_json))
                    if meta_json["product"]["type"] not in UNWANTED_CATEGORIES:
                        category = meta_json["product"]["type"]
                        product_variants = meta_json["product"]["variants"]
                        for pv in product_variants:
                            if "POS ONLY" in pv["name"]: continue
                            if "TRANSPLANT" in pv["public_title"]: continue
                            if "PLANT" in pv["public_title"]: continue
                            if "SEED MAT" in pv["public_title"]: continue
                            if "BARE ROOT" in pv["public_title"]: continue
                            sku = pv["sku"]
                            price = pv["price"] / 100
                            quantity = ""
                            unit = ""
                            logger.debug("Product variant 'public_title': " + pv["public_title"])
                            quantity_and_unit_raw = pv["public_title"].split(" / ")[-1].split(" ")
                            in_stock = None
                            if "SOLD OUT" in pv["public_title"]:
                                in_stock = False
                                quantity_and_unit_raw = quantity_and_unit_raw[:-2]
                            else:
                                in_stock = True
                            if "," in quantity_and_unit_raw:
                                quantity_and_unit_raw.replace(",", "")

                            if "/" in quantity_and_unit_raw[0]:
                                one, two = quantity_and_unit_raw[0].split("/")
                                quantity = str((float(one) / float(two)))
                            elif "/" in quantity_and_unit_raw[1]:
                                acc = float(quantity_and_unit_raw[0])
                                one, two = quantity_and_unit_raw[1].split("/")
                                quantity = str(acc + (float(one) / float(two)))
                            else:
                                quantity = quantity_and_unit_raw[0]

                            unit = quantity_and_unit_raw[-1]

                            url = response.url.split("?")[0] + "?variant=" + str(pv["id"])

                            yield SeedListing(
                                company_prefix = self.company_prefix,
                                company_name = self.company_name,
                                sku = sku,
                                variety = variety,
                                price = price,
                                quantity = quantity,
                                unit = unit,
                                in_stock = in_stock,
                                attributes = {
                                    "category": category,
                                },
                                url = url,
                            )
                else:
                    logger.debug("Meta json is none")
            else:
                logger.debug("Did not find meta var line")
        else:
            logger.debug("Did not find script tag with metadata")

