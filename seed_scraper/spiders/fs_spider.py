import scrapy
import logging
import re
import json

from seed_scraper.items import SeedListing

logger = logging.getLogger(__name__)

class FSSpider(scrapy.Spider):
    '''Scrapes seed product listings from Johnny's Seeds.'''
    company_prefix = "fs"
    company_name = "Fedco Seeds"
    name = company_prefix + "_products"
    base_url = "https://www.fedcoseeds.com/"

    def start_requests(self):
        yield scrapy.Request(url=self.base_url, callback=self.parse)

    def parse(self, response):
        def is_nav_link(response):
            i = re.search("https:\\/\\/www\\.fedcoseeds\\.com\\/.+\\/", response.url) != None and response.url.split("/")[-1] == ''
            if i: logger.debug("Response is nav_link: " + response.url)
            return i

        def is_collection(response):
            i = len(response.css("#bread-crumbs li")) == 2
            if i: logger.debug("Response is collection: " + response.url)
            return i

        def is_product(response):
            i = True
            i = i and not response.url.startswith("https://www.fedcoseeds.com/ogs/")
            i = i and not response.url == "https://www.fedcoseeds.com/seeds/seed-envelopes-5960"
            i = i and not response.url.startswith("https://www.fedcoseeds.com/trees/")
            i = i and len(response.css("#bread-crumbs li")) == 3
            if i: logger.debug("Response is product: " + response.url)
            return i

        logger.debug("Response url is: " + response.url)
        if response.url == self.base_url:
            for i in self.crawl_nav_links(response): yield i
        if is_nav_link(response):
            for i in self.crawl_collections(response): yield i
        if is_collection(response):
            for i in self.crawl_products(response): yield i
        if is_product(response):
            for i in self.parse_product(response): yield i

    def crawl_nav_links(self, response):
        logger.debug("Crawling nav links")
        for c in [c.get() for c in response.xpath("//nav[@id='homeSidenav']/div/a/@href")]:
            yield scrapy.Request(url=self.base_url+c)

    def crawl_collections(self, response):
        logger.debug("Crawling collections from: " + response.url)
        for x in [x.get() for x in response.css(".grid-item a").xpath("@href")]:
            yield scrapy.Request(url=x, callback=self.parse)

        yield None
###        for x in [self.base_url + x.get() for x in response.css("a").xpath("@href")
###                  if x.get().startswith("/collections/")]:
###            print("222")
###            yield scrapy.Request(url=x, callback=self.parse)

    def crawl_products(self, response):
        logger.debug("Crawling products from: " + response.url)
        for x in [x.get() for x in response.xpath("//div[@class='search-results-text']/a/@href")]:
            yield scrapy.Request(url=x, callback=self.parse)

    def parse_product(self, response):
        logger.debug("Parsing product from: " + response.url)
        sku = ""
        sku_span_style = " font-size: medium; font-family: Arial, Helvetica, sans-serif; padding-left: 1em;"
        sku_raw = [x.get() for x in response.xpath("//section/span[@style='" + sku_span_style + "']/text()")][0]
        sku_numstrings = re.search("[0-9]+", sku_raw)
        if sku_numstrings != None:
            sku = sku_numstrings.group()

        variety_raw = [x.get() for x in response.xpath("//h1[@class='product-name']/text()")][0]
        variety_acc = ""
        for x in variety_raw.split(" "):
            if x != '' and x != '\n':
                if variety_acc == "":
                    variety_acc = x
                else:
                    variety_acc += " " + x
        variety = variety_acc

        category = ", ".join([x.get() for x in response.css("#bread-crumbs li a").xpath("text()")])
        for x in [x.get() for x in response.xpath("//h1[@class='product-name']/span/text()")]: variety += " " + x

        for x in [
            x.get()
            for x in response.css("section form div table tr")[1:]
            .css("td.pricecell:nth-child(1)")
            .xpath("text()")
        ]:
            sub_sku = ""
            sub_sku_search = re.search("[A-Z]", x)
            if sub_sku_search != None:
                sub_sku = sub_sku_search.group()

            in_stock = True
            price_search = re.search("[0-9]+\.[0-9]+", x)
            if price_search == None:
                in_stock = False

            price = ""
            quantity = ""
            found_quantity = False
            found_num = False
            unit = ""
            for y in x.split(" "):
                if y.startswith("("):
                    quantity += y.lstrip("(")
                elif y.endswith(")"):
                    quantity += " " + y.rstrip(")")
                    found_quantity = True
                elif not found_quantity and not found_num and re.search("^[0-9].*", y) != None:
                    quantity += re.search("^[0-9].*", y).group().rstrip(" $")
                    found_num = True
                elif re.search("for", y):
                    found_quantity = True
                elif not found_quantity and found_num and y != "":
                    quantity += " " + y
                    found_quantity = True
                elif y.startswith("$"):
                    price = y.lstrip("$").rstrip("\xa0")
                #logger.debug("Quantity split: '" + y + "'")

            quantity = quantity.rstrip("\xa0")
            if quantity.endswith("g"):
                unit = "grams"
                quantity = quantity.rstrip("g")
            elif quantity.endswith(" lb"):
                unit = "lb"
                quantity = quantity.rstrip(" lb")
            elif quantity.endswith("lb"):
                unit = "lb"
                quantity = quantity.rstrip("lb")
            elif quantity.endswith("oz"):
                unit = "oz"
                quantity = quantity.rstrip("oz")
                if "/" in quantity and len(quantity.split("/")) > 1:
                    nums = [int(x) for x in quantity.split("/")]
                    quantity = str(nums[0]/nums[1])
            elif quantity.endswith(" seeds"):
                unit = "seeds"
                quantity = quantity.rstrip(" seeds")
            elif quantity.endswith(" bulbs"):
                unit = "bulbs"
                quantity = quantity.rstrip(" bulbs")
            elif quantity.endswith(" pellets"):
                unit = "pellets"
                quantity = quantity.rstrip(" pellets")

            if unit != None and unit == "" and response.url.startswith("https://www.fedcoseeds.com/bulbs"):
                unit = "bulbs"

            url = response.url

            if sub_sku != None and sub_sku != "":
                yield SeedListing(
                    company_prefix = self.company_prefix,
                    company_name = self.company_name,
                    sku = sku + sub_sku,
                    variety = variety,
                    price = price,
                    quantity = quantity,
                    unit = unit,
                    in_stock = in_stock,
                    attributes = {
                        "category": category,
                    },
                    url = url,
                )
