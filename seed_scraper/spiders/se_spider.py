import scrapy
import logging
import re
import json
from functools import reduce
from pprint import pformat

from seed_scraper.items import SeedListing

logger = logging.getLogger(__name__)

class SESpider(scrapy.Spider):
    company_prefix = "se"
    company_name = "Southern Exposure Seed Exchange"
    name = company_prefix + "_products"
    base_url = "https://southernexposure.com/"
    categories_api_url = "https://southernexposure.com/api/categories/nav/"
    products_api_base_url = "https://southernexposure.com/api/categories/details/"
    products_base_url = "https://southernexposure.com/products/"

    skip_categories = [
        "collections",
        "mixes-blends",
        "mushrooms",
        "books",
        "garden-supplies",
        "seed-saving-supplies",
        "gift-certificates",
    ]

    def start_requests(self):
        yield scrapy.Request(url=self.categories_api_url, callback=self.parse)

    def parse(self, response):
        logger.debug("Response url is: " + response.url)
        if response.url == self.categories_api_url:
            for i in self.crawl_categories(response): yield i
        elif response.url.startswith(self.products_api_base_url):
            for i in self.parse_products(response): yield i

    def crawl_categories(self, response):
        logger.debug("Crawling categories from: " + response.url)
        data = json.loads(response.body)
        #logger.debug("Category data:\n" + pformat(data))
        for cc in data["childrenCategories"].values():
            for c in cc:
                if c["slug"] in self.skip_categories:
                    logger.debug("Skipping category: " + c["name"])
                else:
                    category_api_url = self.products_api_base_url + c["slug"] + "/?perPage=1000"
                    logger.debug("Crawling category: " + c["name"] + " @ " + category_api_url)
                    yield scrapy.Request(url=category_api_url, callback=self.parse)
        yield None

    def parse_products(self, response):
        logger.debug("Parsing product from: " + response.url)
        data = json.loads(response.body)
        category = data["predecessors"][0]["name"]

        for p in data["products"]:
            sku = p["product"]["baseSku"]
            variety = p["product"]["name"]
            seed_attributes = p["seedAttribute"]
            url = self.products_base_url + p["product"]["slug"]

            for v in p["variants"]:
                price = v["price"] / 100
                if "lotSize" in v and v["lotSize"] != None:
                    quantity = v["lotSize"]["size"]
                    unit = v["lotSize"]["type"]
                    if unit == "mass":
                        unit = "g"
                        quantity = quantity / 1000
                    elif unit == "custom":
                        if "Roots" in quantity:
                            unit = "roots"
                            quantity = quantity.split(" ")[0]
                        elif "Rhizomes" in quantity:
                            unit = "rhizomes"
                            quantity = quantity.split(" ")[0]
                        elif "Seedlings" in quantity:
                            unit = "seedlings"
                            quantity = quantity.split(" ")[0]
                        elif "Seeds" in quantity or "seeds" in quantity:
                            unit = "seeds"
                            quantity = quantity.split(" ")[0]
                        elif "lb" in quantity:
                            unit = "lb"
                            quantity = quantity.split(" ")[0]
                    in_stock = v["quantity"] > 0
                    yield SeedListing(
                        company_prefix = self.company_prefix,
                        company_name = self.company_name,
                        sku = sku,
                        variety = variety,
                        price = price,
                        quantity = quantity,
                        unit = unit,
                        in_stock = in_stock,
                        attributes = {
                            "category": category,
                            "seed_attributes": seed_attributes,
                        },
                        url = url,
                    )
        yield None
