import scrapy
import logging
import re
import json
from functools import reduce
from pprint import pformat

from seed_scraper.items import SeedListing

logger = logging.getLogger(__name__)

class SSSpider(scrapy.Spider):
    company_prefix = "ss"
    company_name = "Seed Savers Exchange"
    name = company_prefix + "_products"
    base_url = "https://www.seedsavers.org/"
    collections = [
        "special/from-our-collection"
        "department/vegetable-seeds",
        "department/flower-seeds",
        "department/herb-seeds",
        "organic/organic",
    ]

    def start_requests(self):
        for c in self.collections:
            yield scrapy.Request(url=self.base_url + c + "?show=48", callback=self.parse)

    def parse(self, response):
        logger.debug("Response url is: " + response.url)
        if response.url in [self.base_url + x + "?show=48" for x in self.collections]:
            for i in self.crawl_next_pages(response): yield i
            for i in self.crawl_products(response): yield i
        if response.url.split("?")[0] in [self.base_url + x for x in self.collections]:
            for i in self.crawl_products(response): yield i
        elif "/api/items?" not in response.url:
            for i in self.crawl_product_api(response): yield i
        elif "/api/items?" in response.url:
            for i in self.parse_product(response): yield i

    def crawl_next_pages(self, response):
        pagination_count_raw = response.xpath("//p[@class='global-views-pagination-count']/text()").get()
        if pagination_count_raw != None and " of " in pagination_count_raw:
            logger.debug("Found pagination count: " + pagination_count_raw)
            total_pages = int(pagination_count_raw.split(" of ")[-1])
            logger.debug("Total pages: " + str(total_pages))
            for page_num in range(2, total_pages + 1):
                next_page_url = response.url.split("?")[0] + "?page=" + str(page_num) + "&show=48"
                yield scrapy.Request(url=next_page_url, callback=self.parse)

    def crawl_products(self, response):
        product_links = []
        for x in [x.get() for x in response.xpath("//a[@class='facets-item-cell-grid-title']/@href")]:
            if x not in product_links:
                product_links.append(self.base_url + x[1:])
        logger.debug("Crawling product links:\n" + ("\n").join(product_links))
        for link in product_links:
            yield scrapy.Request(url=link, callback=self.parse)
        yield None

    def crawl_product_api(self, response):
        logger.debug("Crawling product api from: " + response.url)
        api_url = [x.get() for x in response.xpath("//img/@src") if "/api/items?" in x.get()]
        if len(api_url) > 0:
            api_url = self.base_url.rstrip("/") + api_url[0]
            logger.debug("API Url: " + api_url)
            yield scrapy.Request(url=api_url,
                                 callback=self.parse,
                                 meta={"product_url": response.url})
        else:
            logger.debug("API Url: NOT FOUND")
            yield None

    def parse_product(self, response):
        logger.debug("Parsing product from: " + response.url)
        url = response.meta["product_url"]
        json_data = json.loads(response.body)
        if len(json_data["items"]) > 0:
            json_data = json_data["items"][0]
            variety = json_data["displayname"]
            category = ""
            if "custitem_sca_facet_categories" in json_data:
                category = json_data["custitem_sca_facet_categories"]
            if "matrixchilditems_detail" in json_data:
                for listing in json_data["matrixchilditems_detail"]:
                    sku = listing["itemid"]
                    quantity = listing["custitem_sca_seedqty"]
                    unit = ""
                    in_stock = listing["isinstock"]
                    price = ""
                    if in_stock: price = listing["onlinecustomerprice"]

                    quantity = quantity.replace(",", "")

                    if "oz" in quantity:
                        unit = "oz"
                        quantity = quantity.replace("oz", "")
                        if "/" in quantity:
                            one, two = quantity.split("/")
                            quantity = str(float(one)/float(two))
                    if "Packet" in quantity or "seeds" in quantity:
                        unit = "seeds"
                        quantity = quantity.replace("Packet", "").replace("seeds", "").strip(" ")

                    yield SeedListing(
                        company_prefix = self.company_prefix,
                        company_name = self.company_name,
                        sku = sku,
                        variety = variety,
                        price = price,
                        quantity = quantity,
                        unit = unit,
                        in_stock = in_stock,
                        attributes = {
                            "category": category,
                        },
                        url = url,
                    )
            else:
                logger.debug("Product matrixchilditems_detail: NOT FOUND")
        yield None
