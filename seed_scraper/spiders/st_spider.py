import scrapy
import logging
import re
import json
from functools import reduce
from pprint import pformat

from seed_scraper.items import SeedListing

logger = logging.getLogger(__name__)

class STSpider(scrapy.Spider):
    '''Scrapes seed product listings from Johnny's Seeds.'''
    company_prefix = "st"
    company_name = "Sow True Seeds"
    name = company_prefix + "_products"
    base_url = "https://sowtrueseed.com/"
    collections = [
        "collections/vegetables",
        "collections/herb-seeds",
        "collections/flowers",
    ]

    def start_requests(self):
        for c in self.collections:
            yield scrapy.Request(url=self.base_url + c, callback=self.parse)

    def parse(self, response):
        logger.debug("Response url is: " + response.url)
        if response.url.split("?")[0] in [self.base_url + x for x in self.collections]:
            for i in self.crawl_next_page(response): yield i
            for i in self.crawl_products(response): yield i
        elif "/products/" in response.url:
            for i in self.parse_product(response): yield i

    def crawl_products(self, response):
        product_links = []
        for x in [x.get() for x in response.css("div.product-element-top div a").xpath("@href")]:
            if x not in product_links:
                product_links.append(x)
        logger.debug("Crawling product links:\n" + ("\n" + self.base_url).join(product_links))
        for link in product_links:
            yield scrapy.Request(url=self.base_url + link, callback=self.parse)

    def crawl_next_page(self, response):
        if response.css("p.shopify-info").xpath("text()").get() is None:
            logger.debug("Crawling next page link")
            logger.debug("Extracting page number from url")
            url_without_page_attr = response.url.split("?")[0]
            page_num = None
            page_attr = re.search("page=[0-9]+", response.url)
            logger.debug("Url without page attr: " + url_without_page_attr)
            if page_attr != None:
                page_num = int(page_attr.group().lstrip("page="))
                logger.debug("Page number is: " + str(page_num))
            else:
                logger.debug("Page number not found, assuming it is: 1")
                page_num = 1
            next_page_url = url_without_page_attr + "?page=" + str(page_num + 1)
            logger.debug("Crawling next page: " + next_page_url)
            yield scrapy.Request(url=next_page_url, callback=self.parse)

    def parse_product(self, response):
        logger.debug("Parsing product from: " + response.url)
        sku = response.xpath("//span[@id='product-sku']/text()").get()
        variety = response.xpath("//h1[@itemprop='name']/text()").get()
        category = ", ".join([x.get() for x in response.xpath("//span[@class='posted_in']/a/text()")])
        price = response.xpath("//span[@id='product-price']/text()").get()
        if price is None:
            price = response.xpath("//p[@id='productPrice']/ins/text()").get()
        if price is not None:
            price = price.lstrip("$")
        in_stock = response.css("p.out-of-stock").xpath("@style").get() != None and "display: none" in response.css("p.out-of-stock").xpath("@style").get()
        url = response.url

        quantity = ""
        unit = ""
        grams = [
            x.xpath("text()").get().strip("\xa0").split(" ")[0]
            for x in response.css("td")
            if x.xpath("text()").get() != None
            and (
                " gram" in x.xpath("text()").get()
                 or
                " g" in x.xpath("text()").get()
                or
                " gs" in x.xpath("text()").get()
            )
        ]
        if grams != None and len(grams) > 0:
            quantity = grams[0]
            unit = "g"

        yield SeedListing(
            company_prefix = self.company_prefix,
            company_name = self.company_name,
            sku = sku,
            variety = variety,
            price = price,
            quantity = quantity,
            unit = unit,
            in_stock = in_stock,
            attributes = {
                "category": category,
            },
            url = url,
        )
