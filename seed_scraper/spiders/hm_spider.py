import scrapy
import logging
import re
import json

from seed_scraper.items import SeedListing

logger = logging.getLogger(__name__)

class HMSpider(scrapy.Spider):
    '''Scrapes seed product listings from High Mowing Seeds.'''
    company_prefix = "hm"
    company_name = "High Mowing"
    name = company_prefix + "_products"
    base_url = "https://www.highmowingseeds.com/"
    list_pages = [
        "vegetables",
        "flowers",
        "herbs",
        "cover-crops",
        #TODO parse "collections",
    ]

    def start_requests(self):
        urls = [self.base_url + page + ".html?product_list_limit=25"
                for page in self.list_pages]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        def is_product_list_page(response):
            for page in self.list_pages:
                if response.url.startswith(self.base_url + page + ".html"):
                    return True
            return False

        if is_product_list_page(response):
            for i in self.crawl_next_list_page(response): yield i
            for i in self.crawl_product_info(response): yield i
        else:
            for i in self.parse_product_info(response): yield i

    def crawl_next_list_page(self, response):
        '''Finds and crawls link to next page on product lists.'''
        link_el = response.css("li.pages-item-next a")
        if link_el is not None:
            link_url = link_el.attrib.get("href")
            if link_url is not None:
                yield scrapy.Request(link_url, callback=self.parse)

    def crawl_product_info(self, response):
        '''Finds and crawls links to product info pages.'''
        products = response.css("a.product-item-link")[:-1]
        for product in products:
            link = product.attrib.get("href")
            if link is not None:
                yield scrapy.Request(link, callback=self.parse)

    def parse_product_info(self, response):
        sku = response.xpath("//div[@itemprop='sku']/text()").get()
        variety = response.xpath("//span[@itemprop='name']/text()").get()
        in_stock = re.search("Get notified when this product is back in stock",
                             str(response.body)) is None
        price = None
        quantity = None

        if not in_stock:
            yield SeedListing(
                company_prefix = self.company_prefix,
                company_name = self.company_name,
                sku = sku,
                variety = variety,
                price = price,
                quantity = quantity,
                unit = "", # TODO: add unit info
                in_stock = in_stock,
                attributes = {},
                url = response.url,
            )
        elif in_stock:
            data = response.css("#product-options-wrapper")
            data = data.css("script[type='text/x-magento-init']").xpath("text()").get()
            if data is None:
                logger.warning("No price data! " + response.url)
            elif data is not None:
                data = json.loads(data)["#product_addtocart_form"]["configurable"]["spConfig"]
                child_products = data["childProducts"]
                options = list(data["attributes"].values())[0]["options"]
                for x, y in child_products.items():
                    sku = y.get("sku")
                    price = y.get("price")
                    url = self.base_url + y.get("productUrl")
                    quantity = ""
                    unit = ""
                    for o in options:
                        if len(o["products"]) > 0 and o["products"][0] == x:
                            quantity = o["label"]

                    logger.debug("Quantity pre unit filter: " + quantity)
                    if quantity.endswith(" GRAMS"):
                        unit = "g"
                        quantity = quantity.rstrip(" GRAMS")
                    elif quantity.endswith(" GRAM"):
                        unit = "g"
                        quantity = quantity.rstrip(" GRAM")
                    elif quantity.endswith(" OZ"):
                        unit = "oz"
                        quantity = quantity.rstrip(" OZ")
                    elif quantity.endswith(" LB"):
                        unit = "lb"
                        quantity = quantity.rstrip(" LB")
                    elif quantity.endswith(" SEEDS"):
                        unit = "seeds"
                        quantity = quantity.rstrip(" SEEDS")
                    elif quantity.endswith(" SEEDS - Pelleted"):
                        unit = "pelleted seeds"
                        quantity = quantity.rstrip(" SEEDS - Pelleted")
                    elif quantity.endswith(" M"):
                        unit = "m"
                        quantity = quantity.rstrip(" M")
                    elif quantity.endswith(" M - Pelleted"):
                        unit = "m"
                        quantity = quantity.rstrip(" M - Pelleted")
                    logger.debug("Quantity post unit filter: " + quantity)

                    logger.debug("Quantity pre fraction-to-float: " + quantity)
                    if "/" in quantity and len(quantity.split("/")) > 1:
                        nums = [int(x) for x in quantity.split("/")]
                        quantity = str(nums[0]/nums[1])
                    logger.debug("Quantity post fraction-to-float: " + quantity)

                    yield SeedListing(
                        company_prefix = self.company_prefix,
                        company_name = self.company_name,
                        sku = sku,
                        variety = variety,
                        price = price,
                        quantity = quantity,
                        unit = "", # TODO: add unit info
                        in_stock = in_stock,
                        attributes = {},
                        url = response.url,
                    )
