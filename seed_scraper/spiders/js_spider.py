import scrapy
import logging
import re
import json
from functools import reduce

from seed_scraper.items import SeedListing

logger = logging.getLogger(__name__)

class JSSpider(scrapy.Spider):
    '''Scrapes seed product listings from Johnny's Seeds.'''
    company_prefix = "js"
    company_name = "Johnny's Seeds"
    name = company_prefix + "_products"
    base_url = "https://www.johnnyseeds.com/"
    category_pages = [
        "vegetables",
        "fruits",
        "flowers",
        "herbs",
        "farm-seed",
        "organic",
    ]

    def start_requests(self):
        urls = [self.base_url + page + "/?start=0&sz=60"
                for page in self.category_pages]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        def is_product_category_page(response):
            for page in self.category_pages:
                if response.url == self.base_url + page + "/?start=0&sz=60":
                    return True
            return False
        def is_product_subcategory_page(response):
            if is_product_category_page(response): return False
            for page in self.category_pages:
                if response.url.startswith(self.base_url + page + "/") and re.match(".*\.html", response.url) is None:
                    return True

        if is_product_category_page(response):
            for i in self.crawl_next_category_page(response): yield i
            for i in self.crawl_subcategory_page(response): yield i
        elif is_product_subcategory_page(response):
            for i in self.crawl_product_page(response): yield i
        elif re.match(".*\.html", response.url):
            for i in self.parse_product_info(response): yield i

    def crawl_next_category_page(self, response):
        '''Finds and crawls link to next page on product lists.'''
        link_el = response.css("a.c-pagination__link")
        if len(link_el) > 0:
            print(link_el[0].get())
            href_attribute = re.search('href=".*"', link_el[0].get())
            if href_attribute is not None:
                link_url = href_attribute[0].strip("href=").strip('"')
                if link_url is not None:
                    yield scrapy.Request(link_url, callback=self.parse)

    def crawl_subcategory_page(self, response):
        '''Finds and crawls link to subcategory pages.'''
        m = re.search("https:\/\/www\.johnnyseeds\.com\/.+\/", response.url)
        category = m.group().lstrip("https://www.johnnyseeds.com").rstrip("/")
        pages = []
        for a in response.css("a"):
            if "href" in a.attrib and re.match("https:\/\/www\.johnnyseeds\.com\/" + category + "\/.+\/", a.attrib["href"]):
                pages.append(a.attrib["href"])
        def acc_unique(acc, x):
            if x not in acc:
                acc.append(x)
            return acc
        logger.debug("Number of subcategory links:" + str(len(pages)))
        pages = list(reduce(acc_unique, pages, []))
        logger.debug("Number of unique subcategory links:" + str(len(pages)))
        for page in pages:
            yield scrapy.Request(page, callback=self.parse)

    def crawl_product_page(self, response):
        logger.debug("Parsing subcategory page for product info links: " + response.url)
        products = response.css("a.thumb-link")
        for product in products:
            link = product.attrib.get("href")
            if link is not None:
                logger.debug("Crawling product info page: " + link)
                yield scrapy.Request(self.base_url + link, callback=self.parse)

    def parse_product_info(self, response):
        '''Finds and crawls links to product info pages.'''
        logger.debug("Parsing product info: " + response.url)
        sku = response.xpath("//span[@itemprop='productID']/text()").get()
        variety = response.xpath("//h1[@itemprop='name']/text()").get().strip("\n")
        category = response.xpath("//div[@itemprop='alternateName']/text()").get()
        offerings = response.xpath("//form[@itemprop='offers']")
        for o in offerings:
            sku = o.xpath("div[@itemprop='itemOffered']/meta[@itemprop='sku']/@content").get()
            quantity = o.xpath("div[@itemprop='itemOffered']/div/h3/text()").get()
            price = o.xpath("meta[@itemprop='price']/@content").get()
            in_stock = len(o.css("span.in-stock-msg")) is not 0
            url = response.url

            logger.debug("Quantity pre split: " + quantity)
            x = quantity.split(" ")
            unit = ""
            if len(x) > 1:
                quantity = x[0]
                unit = x[1].lower()
            elif len(x) == 1 and x[0] == "Packet":
                quantity = 1
                unit = "packet"

            yield SeedListing(
                company_prefix = self.company_prefix,
                company_name = self.company_name,
                sku = sku,
                variety = variety,
                price = price,
                quantity = quantity,
                unit = unit,
                in_stock = in_stock,
                attributes = {
                    "category": category,
                },
                url = url,
            )
