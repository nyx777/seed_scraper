# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy

class SeedListing(scrapy.Item):
    company_prefix = scrapy.Field()
    company_name = scrapy.Field()
    sku = scrapy.Field()
    variety = scrapy.Field()
    price = scrapy.Field()
    quantity = scrapy.Field()
    unit = scrapy.Field()
    in_stock = scrapy.Field()
    attributes = scrapy.Field()
    url = scrapy.Field()
