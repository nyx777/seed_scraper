#!/usr/bin/env python3
import json
import csv
import os
from pprint import pprint, pformat
from natsort import natsort_keygen, natsorted
from optparse import OptionParser
import logging
import math
import re
from data_loader import load_weight_to_seeds_data, load_company_to_sese_sku_data, load_company_listing_data

logging.basicConfig(level=logging.DEBUG, format="[{name}][{levelname}]: {message}", style="{")
logger = logging.getLogger("data_cleaner")
natsort_key = natsort_keygen()
data_dir = "./data"

def main():
    if options.listings_file == None:
        logger.error("No listings file: exiting")
        return -1
    if options.weight_to_seeds_file == None:
        logger.error("No weight-to-seeds file: exiting")
        return -1
    if options.matches_file == None:
        logger.error("No matches file: exiting")
        return -1

    listings = load_company_listing_data(options.listings_file)
    listings_len = len(listings)
    logger.info("Loaded {} listings from \"{}\"".format(listings_len, options.listings_file))

    logger.info("Discarding listings that do not have quantity information")
    listings = [row for row in listings
                if "quantity" in row
                and row["quantity"] != None
                and row["quantity"] != ""]
    logger.info("    Discarded {} listings, {} listings remain".format(listings_len - len(listings), len(listings)))
    listings_len = len(listings)

    logger.info("Discarding listings that do not have price information")
    listings = [row for row in listings
                if "price" in row
                and row["price"] != None
                and row["price"] != ""]
    logger.info("    Discarded {} listings, {} listings remain".format(listings_len - len(listings), len(listings)))

    logger.info("Converting price data to floating point numbers")
    acc = 0
    for row in listings:
        if type(row["price"]) != float:
            row["price"] = float(str(row["price"]).strip().strip("$"))
            acc += 1
    logger.info("    Converted price data for {} listings".format(acc))

    logger.warning("Removing Hudson Valley \"Art Pack\" listings")
    acc = 0
    for row in [row for row in listings
                if row["company_prefix"] == "hv"
                and "art pack" in str(row["quantity"]).lower()]:
        del listings[listings.index(row)]
        acc += 1
    logger.warning("    Removed {} Hudson Valley \"Art Pack\" listings".format(acc))

    logger.info("Converting quantity data to floating point numbers")
    acc = 0
    for row in listings:
        if type(row["quantity"]) != float:
            search = re.search("[0-9]+\.?([0-9]+)?", str(row["quantity"]))
            if search != None:
                num = float(search.group())
                row["quantity"] = num
                acc += 1
            else:
                del listings[listings.index(row)]

    logger.info("    Converted quantity data for {} listings".format(acc))

    acc = 0
    logger.info("Normalizing \"grams\" to \"g\"")
    for row in listings:
        if "gram" in row["unit"].lower():
            row["unit"] = "g"
            acc += 1
    logger.info("    Normalized unit info for {} listings".format(acc))

    acc = 0
    logger.info("Normalizing \"ounce\" to \"oz\"")
    for row in listings:
        if "ounce" in row["unit"].lower():
            row["unit"] = "oz"
            acc += 1
    logger.info("    Normalized unit info for {} listings".format(acc))

    acc = 0
    logger.info("Normalizing \"pounds\" to \"lb\"")
    for row in listings:
        if "pound" in row["unit"].lower():
            row["unit"] = "lb"
            acc += 1
    logger.info("    Normalized unit info for {} listings".format(acc))

    logger.info("Converting oz to g")
    acc = 0
    oz_to_grams = 28.34952
    for row in listings:
        if row["unit"] == "oz":
            ounces = row["quantity"]
            #logger.info("{} : {}".format(row["sku"], ounces))
            grams = ounces * oz_to_grams
            if "alt_quantity" not in row["attributes"]:
                row["attributes"]["alt_quantity"] = []
            row["attributes"]["alt_quantity"].append({
                "unit": "oz",
                "quantity": ounces,
            })
            row["unit"] = "g"
            row["quantity"] = grams
            acc += 1
    logger.info("    Converted {} listings from oz to g".format(acc))

    logger.info("Converting lb to g")
    acc = 0
    lb_to_grams = 453.5924
    for row in listings:
        if row["unit"] == "lb":
            lbs = row["quantity"]
            grams = lbs * lb_to_grams
            if "alt_quantity" not in row["attributes"]:
                row["attributes"]["alt_quantity"] = []
            row["attributes"]["alt_quantity"].append({
                "unit": "lb",
                "quantity": lbs,
            })
            row["unit"] = "g"
            row["quantity"] = grams
            acc += 1
    logger.info("    Converted {} listings from lb to g".format(acc))

    ##logger.warning("Removing Johnny's listings that have the unit seeds as temporary fix")
    ##acc = 0
    ##for row in [row for row in listings
    ##            if row["company_prefix"] == "js"
    ##            and "seed" in row["unit"]]:
    ##    del listings[listings.index(row)]
    ##    acc += 1
    ##logger.warning("    Removed {} Johnny's listings".format(acc))

    logger.info("Converting seeds to g")
    logger.info("    Loading weight-to-seeds data")
    weight_to_seeds_data = load_weight_to_seeds_data(options.weight_to_seeds_file)
    logger.info("    Loading company-to-sese sku data")
    matches = load_company_to_sese_sku_data(options.matches_file)
    matches_with_seed_data = tuple(m for m in matches
                                   if m[2] in weight_to_seeds_data)
    logger.info("    Loaded {} matches, {} have weight-to-seed data".format(
        len(matches),
        len(matches_with_seed_data)
    ))
    acc = 0
    for match in matches_with_seed_data:
        for row in [row for row in listings
                    if row["company_prefix"] == match[0]
                    and row["sku"] == match[1]
                    and "seed" in row["unit"].lower()]:
            weight_to_seeds = weight_to_seeds_data[match[2]]
            weight_of_one_seed = weight_to_seeds["weight"] / weight_to_seeds["seeds"]
            grams = row["quantity"] * weight_of_one_seed
            if "attributes" not in row or row["attributes"] == None:
                row["attributes"] = {}
            if "alt_quantity" not in row["attributes"]:
                row["attributes"]["alt_quantity"] = []
            row["attributes"]["alt_quantity"].append({
                "unit": "seeds",
                "quantity": row["quantity"]
            })
            row["unit"] = "g"
            row["quantity"] = grams
            acc += 1
    logger.info("    Converted {} listings from seeds to g".format(acc))

    print("\n".join([json.dumps(row) for row in listings]))

if __name__ == "__main__":
    option_parser = OptionParser()
    option_parser.add_option("--lf",
                             help="path to listings file",
                             dest="listings_file",
                             metavar="FILE")
    option_parser.add_option("--wtsf",
                             help="path to weight-to-seeds file",
                             dest="weight_to_seeds_file",
                             metavar="FILE")
    option_parser.add_option("--mf",
                             help="path to matches file",
                             dest="matches_file",
                             metavar="FILE")
    (options, args) = option_parser.parse_args()
    main()
