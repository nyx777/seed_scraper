#!/usr/bin/env bash
echo   '"Company Prefix","Company Name","SKU","Variety","Price","Quantity","Unit","In Stock","URL"'
jq -r '[.company_prefix, .company_name, .sku, .variety, .price, .quantity, .unit, .in_stock, .url] | @csv'
