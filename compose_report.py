#!/usr/bin/env python3
import json
import csv
import os
from pprint import pprint, pformat
from natsort import natsort_keygen, natsorted
from optparse import OptionParser
import logging
import math
from data_loader import load_company_listing_data, load_company_to_sese_sku_data

logging.basicConfig(level=logging.DEBUG, format="[{name}][{levelname}]: {message}", style="{")
logger = logging.getLogger("compose_report")
natsort_key = natsort_keygen()
data_dir = "./data"

def main():
    if options.matches_file == None:
       logger.error("No matches file: exiting")
       return -1
    if options.listings_file == None:
       logger.error("No listings file: exiting")
       return -1

    logger.info("Loading listing data...")
    company_listing_data = load_company_listing_data(options.listings_file)
    company_listing_data = [row for row in company_listing_data
                              if row["quantity"] != ""
                              if row["price"] != ""]
    logger.info("    Loaded {} listings from {} companies".format(
          len(company_listing_data),
          len(set(row["company_prefix"] for row in company_listing_data))))

    all_sese_varieties = {}
    for row in [row for row in company_listing_data if row["company_prefix"] == "se"]:
         if row["sku"] not in all_sese_varieties:
            all_sese_varieties[row["sku"]] = []
         all_sese_varieties[row["sku"]].append(row)
    sese_varieties_smallest_sizes = [[row for row in sorted(rows, key=lambda row: row["price"])][0]
                                       for sku, rows in all_sese_varieties.items()]
    logger.info("    Loaded {} SESE listings".format(len(sese_varieties_smallest_sizes)))

    logger.info("Loading company-to-sese sku-data...")
    company_to_sese_sku_data = load_company_to_sese_sku_data(options.matches_file)
    company_to_sese_sku_data = tuple(row_x for row_x in company_to_sese_sku_data
                                       if row_x[2] in [row_y["sku"] for row_y in sese_varieties_smallest_sizes])
    logger.info("    Loaded {} matches from {} companies for {} SESE varieties".format(
          len(company_to_sese_sku_data),
          len(set(row[0] for row in company_to_sese_sku_data)),
          len(set(row[2] for row in company_to_sese_sku_data))))

    # filter only matches that have listing data in grams
    def listing_unit(sku):
         dat =  [row for row in company_listing_data
                 if row["sku"] == sku]
         if dat == None or len(dat) == 0: return ""
         return dat[0]["unit"]
    company_to_sese_sku_data = tuple(row for row in company_to_sese_sku_data
                                       if listing_unit(row[1]) == "g"
                                       or listing_unit(row[1]).lower() == "gram"
                                       or listing_unit(row[1]).lower() == "grams")

    logger.info("    Loaded {} matches in grams from {} companies for {} SESE varieties".format(
          len(company_to_sese_sku_data),
          len(set(row[0] for row in company_to_sese_sku_data)),
          len(set(row[2] for row in company_to_sese_sku_data))))

    # accumulate match data
    output_data = {}
    for match in company_to_sese_sku_data:
         sese_row = [row for row in sese_varieties_smallest_sizes if row["sku"] == match[2]][0]
         company_row = [row for row in company_listing_data
                        if row["company_prefix"] == match[0]
                        and row["sku"] == match[1]]
         if len(company_row) > 0:
            company_row = company_row[0]
         else:
            continue
         if match[2] not in output_data:
            output_data[match[2]] = {"sese_row": sese_row, "company_rows": []}
         output_data[match[2]]["company_rows"].append(company_row)

    # find closest weight match by company
    def closest_weight(sese_row, company_rows):
        weight_difference = [(index, math.sqrt((float(sese_row["quantity"]) - float(company_row["quantity"]))**2))
                             for index, company_row in list(zip(range(len(company_rows)), company_rows))]
        if len(weight_difference) > 0:
           weight_difference.sort()
        return weight_difference[0][0]
    output_data_weight_match = {}
    def add_all(list_of_lists):
        acc = []
        for l in list_of_lists:
            acc += l
        return acc
    for sese_sku, data in output_data.items():
        for company_prefix in set(row["company_prefix"] for row in data["company_rows"]):
           matches = [row for row in data["company_rows"] if row["company_prefix"] == company_prefix]
           closest_weight_match = matches[closest_weight(data["sese_row"], matches)]
           if closest_weight_match != None:
               if sese_sku not in output_data_weight_match:
                  output_data_weight_match[sese_sku] = {"sese_row": data["sese_row"]}
               if "company_rows" not in output_data_weight_match[sese_sku]:
                  output_data_weight_match[sese_sku]["company_rows"] = []
               output_data_weight_match[sese_sku]["company_rows"].append(closest_weight_match)
           else:
               logger.error("Closest weight match is None unexpectedly")
    output_data = output_data_weight_match

    # create ordered list of the sese skus that have the most matches
    num_company_rows = set()
    for sese_sku, data in output_data.items():
         num_company_rows.add((len(data["company_rows"]), sese_sku))
    num_company_rows = sorted(list(num_company_rows), key=lambda x: x[0])
    num_company_rows.reverse()

    logger.info("Correlated {} matches from {} companies for {} SESE varieties".format(
       sum([len(data["company_rows"]) for data in output_data.values()]),
       len(set(add_all([[row["company_prefix"] for row in rows] for rows in [data["company_rows"] for data in output_data.values()]]))),
       len(output_data.keys())))
    print("Data from {} matches from {} companies for {} SESE varieties".format(
       sum([len(data["company_rows"]) for data in output_data.values()]),
       len(set(add_all([[row["company_prefix"] for row in rows] for rows in [data["company_rows"] for data in output_data.values()]]))),
       len(output_data.keys())))
    print()

    company_price_differentials = {}
    for sese_sku, data in output_data.items():
       sese_row = data["sese_row"]
       for row in data["company_rows"]:
          if row["company_name"] not in company_price_differentials:
             company_price_differentials[row["company_name"]] = []
          company_price_differentials[row["company_name"]].append( ( ( (float(row["price"])/float(row["quantity"])) / (float(sese_row["price"])/float(sese_row["quantity"])) ) ) )
    print('"Company","Matches","Average Price Differential"')
    for company_name, differentials in sorted(company_price_differentials.items(),
                                              reverse=True,
                                              key=lambda x: sum(x[1])/len(x[1])):
       print('"{name}","{num_matches}","{avg}"'.format(name=company_name, num_matches=len(differentials), avg=(sum(differentials)/len(differentials)) ))
    print()

    def format_row(sese_row, row):
        data_format_string = '"{sese_sku}","{variety}","{company}","{sku}","{price}","{quantity}","{unit}","{price_per_unit}","{percent_price_difference_from_sese}","{url}"'
        return data_format_string.format(
           sese_sku = sese_row["sku"],
           variety = sese_row["variety"],
           company = row["company_name"],
           sku = row["sku"],
           price = row["price"],
           quantity = row["quantity"],
           unit = "g",
           price_per_unit = float(row["price"])/float(row["quantity"]),
           percent_price_difference_from_sese = ( ( (float(row["price"])/float(row["quantity"])) / (float(sese_row["price"])/float(sese_row["quantity"])) ) ),
           url = row["url"],
        )
    header = '"SESE SKU","Variety","Company","SKU","Price","Quantity","Unit","Price / Unit Quantity","% Higher Price Than SESE","URL"'
    empty_row = '"","","","","","","","","","",'
    print(header)
    print(empty_row)
    for _, sese_sku in num_company_rows:
         print(format_row(output_data[sese_sku]["sese_row"], output_data[sese_sku]["sese_row"]))
         for row in sorted(output_data[sese_sku]["company_rows"],
                           key=lambda row: float(row["price"])/float(row["quantity"])):
            print(format_row(output_data[sese_sku]["sese_row"], row))
         print(empty_row)

if __name__ == "__main__":
    option_parser = OptionParser()
    option_parser.add_option("--mf",
                             help="path to matches files",
                             dest="matches_file",
                             metavar="FILE")
    option_parser.add_option("--lf",
                             help="path to listings files",
                             dest="listings_file",
                             metavar="FILE")
    (options, args) = option_parser.parse_args()
    main()
